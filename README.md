
# Ascii Map Sniffer

## Short description

This application analyzes ASCII map as an input and outputs the letters and the path that the algorithm travelled.
It starts from a starting sign(@) and ends at ending sign(x). This solution uses a greedy algorithm to find an ending sign.
It does so in a way that it uses information about direction and current sign and possible next sign to determine concrete next sign.


## Example

```
  @
  | C----+
  A |    |
  +---B--+
    |      x
    |      |
    +---D--+
```

Expected result: 
- Letters ```ABCD```
- Path as characters ```@|A+---B--+|+----C|-||+---D--+|x```

## Instructions for starting and using the application

This map sniffer uses Maven for project lifecycle management. You can find instructions in section "Installing Maven".
Alternatively you can use NetBeans as an IDE and create a configuration for running the application.

### Running the application with Maven:

The app can be run from the root of the application( where pom.xml is found ) with command

```mvn exec:java -Dexec.mainClass=com.softwaresauna.asciimap.AsciiMapApplication -Dexec.args="res/map1.txt res/maven_test.txt"```

### Running tests

Maven can be used to run tests of application with command

```mvn test```

Or if you want to run a specific test

```mvn -Dtest=AsciiMapRecieverTest test```

### Running the application with NetBeans:

Download and install Netbeans. You can find instructions here https://netbeans.org/

- Go to : Set project configuration -> Customize
- Go to arguments field enter: "res/map.txt" "res/netbeans_output.txt"
- "res/map.txt" "res/netbeans_output.txt" are provided arguments to the console application.
- If Netbeans requires to choose the main class, you must provide a path to AsciiMapApplication.java
-  This is important: you must provide file defined in the first argument with actual ascii map, currently there are 3 maps in res folder you can use those
- Run -> Run Project or use shortcut ctrl + F11


## Installing Maven

Apache Maven is a software project management and comprehension tool. Based on the concept of a project object model (POM),
Maven can manage a project's build, reporting and documentation from a central piece of information.

You can find instructions of Maven here: https://maven.apache.org/install.html
