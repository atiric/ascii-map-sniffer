package com.softwaresauna.asciimap;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class AsciiMapApplication {

    public static final int EXPECTED_NUM_ARG = 2;

    public static final int INVALID_STATE_ARGUMENTS = -1;

    public static final int UNEXPECTED_STATE = -2;

    /**
     * Main method to be executer. Expected number of parameters is two.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length != EXPECTED_NUM_ARG) {
            System.out.println("Two parameters are required: input map path and output file path");
            System.exit(INVALID_STATE_ARGUMENTS);
        } else if (args.length == EXPECTED_NUM_ARG) {
            String inputFileDescription = args[0];
            String outputFileDescription = args[1];
            Path input = Paths.get(inputFileDescription);
            Path output = Paths.get(outputFileDescription);
            String[] mapLines = null;
            try {
                mapLines =  Files.readAllLines(input).toArray(new String[0]);
            } catch (IOException ex) {
                System.out.println("Can't read file " + args[0]);
                System.exit(UNEXPECTED_STATE);
            }
            AsciiMapSniffer mapSniffer = new AsciiMapSniffer(mapLines, output);
            mapSniffer.startSniffing();
            // I want to see test running, System.exit exits VM
            return;
        }
        System.exit(UNEXPECTED_STATE);
    }
    
}
