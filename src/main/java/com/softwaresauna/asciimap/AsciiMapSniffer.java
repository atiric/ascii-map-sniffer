package com.softwaresauna.asciimap;

import com.softwaresauna.asciimap.implementation.AsciiMapImpl;
import com.softwaresauna.asciimap.implementation.AsciiMapReciever;
import com.softwaresauna.asciimap.implementation.AsciiMapInvokerImpl;
import com.softwaresauna.asciimap.implementation.command.DetermineDirectionCommand;
import com.softwaresauna.asciimap.implementation.command.MoveOnMapCommand;
import com.softwaresauna.asciimap.implementation.command.SaveCharacterCommand;
import com.softwaresauna.asciimap.model.AsciiMap;
import com.softwaresauna.asciimap.model.AsciiMapInvoker;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * This class represents Client in Command design pattern. It will create and
 * initialize Commands and AsciiMap model. The Client creates and configures
 * concrete command objects. The client must pass all of the request parameters,
 * including a receiver instance, into the command’s constructor.
 *
 * @author Armin
 */
public class AsciiMapSniffer {

    private final AsciiMapReciever asciiMapReciever;
    private final AsciiMapInvoker mapInvoker;
    private final Path output;

    AsciiMapSniffer(String[] mapLines, Path output) {
        AsciiMap map = new AsciiMapImpl(mapLines);
        this.asciiMapReciever = new AsciiMapReciever(map);
        this.mapInvoker = new AsciiMapInvokerImpl();
        this.output = output;
    }

    public void startSniffing(){    
        char currentChar = asciiMapReciever.getCurrentCharacter();
        while (currentChar != AsciiMap.WANTED_CHARACTER) {
            mapInvoker.setAsciiMapCommand(
                    new DetermineDirectionCommand(asciiMapReciever))
                    .executeCommand()
                    .setAsciiMapCommand(new SaveCharacterCommand(asciiMapReciever))
                    .executeCommand()
                    .setAsciiMapCommand(new MoveOnMapCommand(asciiMapReciever))
                    .executeCommand();
            currentChar = asciiMapReciever.getCurrentCharacter();
        }
        mapInvoker.setAsciiMapCommand(new SaveCharacterCommand(asciiMapReciever))
                .executeCommand();

        writeToOutputPath(output, asciiMapReciever.getCharacterPath(), asciiMapReciever.getPath());
        System.out.println("Letters " + asciiMapReciever.getCharacterPath());
        System.out.println("Path as characters " + asciiMapReciever.getPath());
    }

    private void writeToOutputPath(Path output, String characterPath, String fullPath) {
        String lines = characterPath + "\n" + fullPath;
        try {
            Files.write(
                    output,
                    lines.getBytes(),
                    StandardOpenOption.TRUNCATE_EXISTING,
                    StandardOpenOption.WRITE,
                    StandardOpenOption.CREATE);
        } catch (IOException ex) {
            System.out.println("Can't write file " + output);
        }
    }

}
