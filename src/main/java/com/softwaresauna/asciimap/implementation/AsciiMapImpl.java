package com.softwaresauna.asciimap.implementation;

import com.softwaresauna.asciimap.model.AsciiMap;
import com.softwaresauna.asciimap.model.Coordinate;
import com.softwaresauna.asciimap.model.Direction;

/**
 * Class representing concrete AsciiMap implementation.
 * @author Armin
 */
public class AsciiMapImpl implements AsciiMap {

    String[] map;

    Direction direction;
    
    Coordinate<Integer> currentCoordinate;

    public AsciiMapImpl(String[] map) {
        this.map = map;
        this.direction = null;
        determineCoordinate();
    }

    private void determineCoordinate() {
        int x = 0, y = 0;
        for (int row = 0; row < map.length; row++) {
            if (rowContainsStartingCharacter(row)) {
                x = row;
                y = map[row].indexOf(AsciiMap.STARTING_CHARACTER.toString());
                break;
            }
        }
        this.currentCoordinate = new Coordinate(x, y);
    }
    
    
    boolean rowContainsStartingCharacter(int row) {
        return map[row].contains(AsciiMap.STARTING_CHARACTER.toString());
    }

    @Override
    public String[] getAsciiMap() {
        return map;
    }

    @Override
    public Direction getCurrentDirection() {
        return direction;
    }

    @Override
    public AsciiMap setCurrentDirection(Direction direction) {
        this.direction = direction;
        return this;
    }

    @Override
    public Coordinate<Integer> getCurrentCoordinate() {
        return currentCoordinate;
    }

    @Override
    public AsciiMap setCurrentCoordinate(Coordinate<Integer> coordinate) {
        this.currentCoordinate = coordinate;
        return this;
    }

}
