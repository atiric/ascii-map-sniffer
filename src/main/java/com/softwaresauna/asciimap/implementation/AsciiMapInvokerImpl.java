package com.softwaresauna.asciimap.implementation;

import com.softwaresauna.asciimap.model.AsciiMapInvoker;
import com.softwaresauna.asciimap.model.AsciiCommand;

/**
 * Concrete invoker of commands;
 * @author Armin
 */
public class AsciiMapInvokerImpl implements AsciiMapInvoker {
    
    private AsciiCommand command;

    @Override
    public AsciiMapInvoker setAsciiMapCommand(AsciiCommand command) {
        this.command = command;
        return this;
    }

    @Override
    public AsciiMapInvoker executeCommand() {
        this.command.execute();
        return this;
    }
    
}
