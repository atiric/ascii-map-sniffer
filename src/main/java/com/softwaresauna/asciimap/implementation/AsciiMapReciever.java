package com.softwaresauna.asciimap.implementation;

import com.softwaresauna.asciimap.model.AsciiMap;
import com.softwaresauna.asciimap.model.Coordinate;
import com.softwaresauna.asciimap.model.CurrentCharacterHolder;
import com.softwaresauna.asciimap.model.Direction;
import java.util.HashMap;
import java.util.Map;

/**
 * Class that will do business logic of determining the path and characters
 * based on command received from start to end character.
 *
 * @author Armin
 */
public class AsciiMapReciever implements CurrentCharacterHolder {

    private final AsciiMap asciiMap;

    private StringBuilder mapPath;
    private StringBuilder mapAlphabetPath;
    private Map<Coordinate<Integer>, Character> savedCoordinateCharaters;

    public AsciiMapReciever(AsciiMap asciiMap) {
        this.asciiMap = asciiMap;
        mapPath = new StringBuilder();
        mapAlphabetPath = new StringBuilder();
        savedCoordinateCharaters = new HashMap<>();
    }

    public Direction determineDirection() {
        char currentChar = getCurrentCharacter();
        Direction futureDirection = null;
        switch (currentChar) {
            case '@':
                return initializeDirectionOfSearch();
            case '|':
                futureDirection = sniffPathUpAndDown();
                if( futureDirection == null ) {
                    futureDirection = sniffPathRightAndLeft();
                }
                return futureDirection == null?
                        determineTurningDirection(): futureDirection;
            case '-':
                futureDirection = sniffPathRightAndLeft();
                if( futureDirection == null ) {
                    futureDirection = sniffPathUpAndDown();
                }
                return futureDirection == null ? determineTurningDirection(): futureDirection;
            case '+':
                return determineTurningDirection();
            default:
                return determineAlphanumericDirection(currentChar);
        }
    }

    @Override
    public char getCurrentCharacter() {
        Coordinate<Integer> currentCoordinate = asciiMap.getCurrentCoordinate();
        Direction direction = asciiMap.getCurrentDirection();
        int row = currentCoordinate.getX();
        int col = currentCoordinate.getY();
        return asciiMap.getAsciiMap()[row].charAt(col);
    }

    private Direction initializeDirectionOfSearch() {
        Coordinate<Integer> currentCoordinate = asciiMap.getCurrentCoordinate();
        int row = currentCoordinate.getX();
        int col = currentCoordinate.getY();
        char currentChar = getCurrentCharacter();
        String rowOfMap = asciiMap.getAsciiMap()[row];
        String[] asciiMapRows = asciiMap.getAsciiMap();
        if (conditionForRightSniffValid(rowOfMap, col, currentChar)) {
            return Direction.RIGHT;
        } else if (conditionForLeftSniffValid(rowOfMap, col, currentChar)) {
            return Direction.LEFT;
        } else if (conditionForUppSniffValid(col, row, asciiMapRows, currentChar)) {
            return Direction.UP;
        } else if (conditionForDownSniffValid(col, row, asciiMapRows, currentChar)) {
            return Direction.DOWN;
        }
        throw new IllegalStateException("WTF!");
    }

    private Direction determineAlphanumericDirection(char currentChar) {
        if (Character.isAlphabetic(currentChar)) {
            Direction direction = sniffPathRightAndLeft();
            if (direction == null) {
                direction = sniffPathUpAndDown();
            }
            if (direction == null) {
                direction = determineTurningDirection();
            }
            return direction;

        }
        throw new IllegalStateException("Can't determine alphabetic direction");
    }

    private Direction determineTurningDirection() {
        Coordinate<Integer> currentCoordinate = asciiMap.getCurrentCoordinate();
        int row = currentCoordinate.getX();
        int col = currentCoordinate.getY();
        char currentChar = getCurrentCharacter();
        String rowOfMap = asciiMap.getAsciiMap()[row];
        String[] asciiMapRows = asciiMap.getAsciiMap();
        Direction currentDirection = asciiMap.getCurrentDirection();
        if (conditionForRightSniffValid(rowOfMap, col, currentChar)
                && currentDirection != Direction.LEFT) {
            return Direction.RIGHT;
        } else if (conditionForLeftSniffValid(rowOfMap, col, currentChar)
                && currentDirection != Direction.RIGHT) {
            return Direction.LEFT;
        } else if (conditionForUppSniffValid(col, row, asciiMapRows, currentChar)
                && currentDirection != Direction.DOWN) {
            return Direction.UP;
        } else if (conditionForDownSniffValid(col, row, asciiMapRows, currentChar)
                && currentDirection != Direction.UP) {
            return Direction.DOWN;
        }
        throw new IllegalStateException("WTF Turning!");
    }

    private Direction sniffPathRightAndLeft() {
        Coordinate<Integer> currentCoordinate = asciiMap.getCurrentCoordinate();
        Direction currentDirection = asciiMap.getCurrentDirection();
        int row = currentCoordinate.getX();
        int col = currentCoordinate.getY();
        char currentChar = getCurrentCharacter();
        String rowOfMap = asciiMap.getAsciiMap()[row];
        if (currentDirection == Direction.RIGHT) {
            if (conditionForRightSniffValid(rowOfMap, col, currentChar)) {
                return Direction.RIGHT;
            }
        } else if (currentDirection == Direction.LEFT) {
            if (conditionForLeftSniffValid(rowOfMap, col, currentChar)) {
                return Direction.LEFT;
            }
        }
        //nothing to sniff
        return null;
    }

    private boolean conditionForRightSniffValid(String rowOfMap, int col, char currentChar) {
        if (col + 1 < rowOfMap.length()) {
            return rowOfMap.charAt(col + 1) == '-'
                    || ( currentChar == '-' && rowOfMap.charAt(col + 1) == '|' )
                    || rowOfMap.charAt(col + 1) == '+'
                    || Character.isAlphabetic(rowOfMap.charAt(col + 1));
        }
        return false;

    }

    private boolean conditionForLeftSniffValid(String rowOfMap, int col, char currentChar) {
        if (col - 1 >= 0) {
            return rowOfMap.charAt(col - 1) == '-'
                    || ( currentChar == '-' && rowOfMap.charAt(col - 1) == '|' )
                    || rowOfMap.charAt(col - 1) == '+'
                    || Character.isAlphabetic(rowOfMap.charAt(col - 1));
        }
        return false;
    }

    private Direction sniffPathUpAndDown() {
        Coordinate<Integer> currentCoordinate = asciiMap.getCurrentCoordinate();
        Direction currentDirection = asciiMap.getCurrentDirection();
        int row = currentCoordinate.getX();
        int col = currentCoordinate.getY();
        char currentChar = getCurrentCharacter();
        String[] asciiMapRows = asciiMap.getAsciiMap();
        if (currentDirection == Direction.UP) {
            if (conditionForUppSniffValid(col, row, asciiMapRows,currentChar)) {
                return Direction.UP;
            }
        } else if (currentDirection == Direction.DOWN) {
            if (conditionForDownSniffValid(col, row, asciiMapRows, currentChar)) {
                return Direction.DOWN;
            }

        }
        return null;
    }

    boolean conditionForUppSniffValid(int col, int row, String[] asciiMapRows, char currentChar) {
        if (row - 1 >= 0 && col < asciiMapRows[row - 1].length()) {
            char upperChar = asciiMapRows[row - 1].charAt(col);
            return upperChar == '|'
                    || ( currentChar == '|' && upperChar == '-' )
                    || upperChar == '+'
                    || Character.isAlphabetic(upperChar);

        }
        return false;
    }

    boolean conditionForDownSniffValid(int col, int row, String[] asciiMapRows, char currentChar) {
        if (row + 1 != asciiMapRows.length && col < asciiMapRows[row + 1].length() ) {
            char upperChar = asciiMapRows[row + 1].charAt(col);
            return upperChar == '|'
                    || ( currentChar == '|' && upperChar == '-' )
                    || upperChar == '+'
                    || Character.isAlphabetic(upperChar);
        }
        return false;
        
    }

    public void setMapDirection(Direction direction) {
        this.asciiMap.setCurrentDirection(direction);
    }

    public void moveInCurrentDirection() {
        Direction currentDirection = asciiMap.getCurrentDirection();
        Coordinate<Integer> currentCoordinate = asciiMap.getCurrentCoordinate();
        switch (currentDirection) {
            case DOWN:
                currentCoordinate.setX(currentCoordinate.getX() + 1);
                break;
            case UP:
                currentCoordinate.setX(currentCoordinate.getX() - 1);
                break;
            case LEFT:
                currentCoordinate.setY(currentCoordinate.getY() - 1);
                break;
            case RIGHT:
                currentCoordinate.setY(currentCoordinate.getY() + 1);
                break;
        }
    }

    public void saveCurrentCharacter() {
        char currentChar = getCurrentCharacter();
        mapPath.append(currentChar);
        if (Character.isAlphabetic(currentChar) && currentChar != AsciiMap.WANTED_CHARACTER) {
            if( !savedCoordinateCharaters.containsKey(asciiMap.getCurrentCoordinate())){
                saveCurrentCoordinateOfCharacter(currentChar);
                mapAlphabetPath.append(currentChar);
            }
        }
    }

    public String getCharacterPath() {
        return mapAlphabetPath.toString();
    }

    public String getPath() {
        return mapPath.toString();
    }

    private void saveCurrentCoordinateOfCharacter(char currentChar) {
        Coordinate<Integer> copyOfCurrentCoordinate = asciiMap.getCurrentCoordinate().copy();
        savedCoordinateCharaters.put(copyOfCurrentCoordinate, currentChar);
    }
}
