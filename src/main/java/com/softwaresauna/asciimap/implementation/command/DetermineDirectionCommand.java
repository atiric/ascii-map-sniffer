package com.softwaresauna.asciimap.implementation.command;

import com.softwaresauna.asciimap.model.AsciiCommand;
import com.softwaresauna.asciimap.model.AsciiMap;
import com.softwaresauna.asciimap.implementation.AsciiMapReciever;
import com.softwaresauna.asciimap.model.Direction;

/**
 *
 * @author Armin
 */
public class DetermineDirectionCommand implements AsciiCommand {
    
    AsciiMapReciever asciiMapReciever;

    public DetermineDirectionCommand(AsciiMapReciever asciiMapReciever) {
        this.asciiMapReciever = asciiMapReciever;
    }
    
    @Override
    public void execute() {
        Direction direction = asciiMapReciever.determineDirection();
        asciiMapReciever.setMapDirection(direction);
    }
    
}
