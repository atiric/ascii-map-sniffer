/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.softwaresauna.asciimap.implementation.command;

import com.softwaresauna.asciimap.implementation.AsciiMapReciever;
import com.softwaresauna.asciimap.model.AsciiCommand;

/**
 *
 * @author Armin
 */
public class MoveOnMapCommand implements AsciiCommand {
    
    AsciiMapReciever asciiMapReciever;

    public MoveOnMapCommand(AsciiMapReciever asciiMapReciever) {
        this.asciiMapReciever = asciiMapReciever;
    }

    @Override
    public void execute() {
        this.asciiMapReciever.moveInCurrentDirection();
    }
    
}
