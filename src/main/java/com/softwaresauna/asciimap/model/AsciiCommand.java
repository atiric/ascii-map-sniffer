package com.softwaresauna.asciimap.model;

/**
 * Interface for AsciiCommand that will pass the call and parameters to a AsciiMapReciever.
 * @author Armin
 */
public interface AsciiCommand {
    
    /**
     * Main method that will call appropriate methods on AsciiMapReciever.
     */
    void execute();

}
