package com.softwaresauna.asciimap.model;

/**
 * Model of ascii map based on array of String for easier character/string manipulation.
 * @author Armin
 */
public interface AsciiMap {
    
    /**
     * Getter for array of ascii map.
     * @return Array of strings representing rows of map
     */
    String[] getAsciiMap();
    /**
     * Getter for current direction of map. When initializing map this should be null.
     * @return Direction of map from current coordinate.
     */
    Direction getCurrentDirection();
    /**
     * Setter for current direction of map.
     * @param direction Direction to be set.
     * @return this map.
     */
    AsciiMap setCurrentDirection(Direction direction);
    /**
     * Getter for current coordinate on map.
     * @return Current coordinate.
     */
    Coordinate<Integer> getCurrentCoordinate();
    /**
     * Setter for current coordinate.
     * @param coordinate Coordinate to be set.
     * @return this map.
     */
    AsciiMap setCurrentCoordinate(Coordinate<Integer> coordinate);
    /**
     * Starting character on map. Search will start at this character.
     * There can be only one starting character on map.
     */
    static final Character STARTING_CHARACTER = '@';
    /**
     * Ending character on map. Search will end at this character.
     * There can be only one ending character on map.
     */
    static final Character WANTED_CHARACTER = 'x';
    
    
}
