package com.softwaresauna.asciimap.model;

/**
 * The AsciiMapInvoker is responsible for initiating requests.
 * This class must have a field for storing a reference to a command object.
 * The sender triggers that command instead of sending the request directly to the receiver.
 * Note that the sender isn’t responsible for creating the command object. 
 * @author Armin
 */
public interface AsciiMapInvoker {
    
    /**
     * Setter for asciiMap commands
     * @param command Command to be executed later.
     * @return  This invoker for fluent interface method chaining.
     */
    AsciiMapInvoker setAsciiMapCommand(AsciiCommand command);
    /**
     * Start execution of command.
     * @return This invoker for fluent interface method chaining
     */
    AsciiMapInvoker executeCommand();
    
}
