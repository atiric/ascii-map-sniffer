package com.softwaresauna.asciimap.model;

import java.util.Objects;

/**
 * Class representing coordinate on map.
 * @author Armin
 */
public class Coordinate<Type> {
    /**
     * X coordinate on map.
     */
    public Type x;
    /**
     * X coordinate on map.
     */
    public Type y;

    /**
     * Initializes new coordinate with typed parameters.
     * @param x First parameter.
     * @param y Second parameter.
     */
    public Coordinate(Type x, Type y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Getter for first parameter.
     * @return First parameter.
     */
    public Type getX() {
        return x;
    }

    /**
     * Setter for first parameter,
     * @param x Parameter to be set.
     */
    public void setX(Type x) {
        this.x = x;
    }
    
    /**
     * Getter for first parameter.
     * @return First parameter.
     */
    public Type getY() {
        return y;
    }

    /**
     * Setter for second parameter.
     * @param y Second parameter.
     */
    public void setY(Type y) {
        this.y = y;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.x);
        hash = 29 * hash + Objects.hashCode(this.y);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coordinate<?> other = (Coordinate<?>) obj;
        if (!Objects.equals(this.x, other.x)) {
            return false;
        }
        if (!Objects.equals(this.y, other.y)) {
            return false;
        }
        return true;
    }
    
    public Coordinate copy(){
        return new Coordinate(x, y);
    } 
    

    @Override
    public String toString() {
        return "Coordinate{" + "x=" + x + ", y=" + y + '}';
    }
    
}
