package com.softwaresauna.asciimap.model;

/**
 * Interface defining class that has information of current character.
 * This interface is important because it is used for termination of search condition.
 * @author Armin
 */
public interface CurrentCharacterHolder {
    /**
     * Getter for current character.
     * @return Current character.
     */
    char getCurrentCharacter();
    
}
