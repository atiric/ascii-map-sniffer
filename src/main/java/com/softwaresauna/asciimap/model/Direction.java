package com.softwaresauna.asciimap.model;

/*
 * Enum that defines current direction of walking trough AsciiMap
 * @author Armin
 */
public enum Direction {
    UP, DOWN, LEFT, RIGHT    
}
