package com.softwaresauna.asciimap;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

/**
 * Using naming convention [the name of the tested method]_[expected input /
 * tested state]_[expected behavior].
 *
 * @author Armin
 */
public class AsciiMapSnifferTest {

    public static final String TEST_OUTPUT_URL = "res/test_output.txt";

    @Rule
    public Timeout globalTimeout = Timeout.seconds(2);

    @Test
    public void startSniffing_firstMapUseCaseValid_shouldMatchLettersAndPath() throws IOException {
        String[] mapLines = fetchMapLines("res/map1.txt");
        assertTrue(mapLines != null);
        Path outputPath = Paths.get(TEST_OUTPUT_URL);
        AsciiMapSniffer asciiMapSniffer = new AsciiMapSniffer(mapLines, outputPath);
        asciiMapSniffer.startSniffing();
        try {
            List<String> outputLines = Files.readAllLines(
                    outputPath,
                    Charset.defaultCharset());
            assertTrue(outputLines.size() == 2);
            assertEquals(outputLines.get(0).trim(), "ACB");
            assertEquals(outputLines.get(1).trim(), "@---A---+|C|+---+|+-B-x");
        } catch (IOException ex) {
            // do nothing
        }

    }
    
    @Test
    public void startSniffing_secondMapUseCaseValid_shouldMatchLettersAndPath() {
        String[] mapLines = fetchMapLines("res/map2.txt");
        assertTrue(mapLines != null);
        Path outputPath = Paths.get(TEST_OUTPUT_URL);
        AsciiMapSniffer asciiMapSniffer = new AsciiMapSniffer(mapLines, outputPath);
        asciiMapSniffer.startSniffing();
        try {
            List<String> outputLines = Files.readAllLines(
                    outputPath,
                    Charset.defaultCharset());
            assertTrue(outputLines.size() == 2);
            assertEquals(outputLines.get(0).trim(), "ABCD");
            assertEquals(outputLines.get(1).trim(), "@|A+---B--+|+----C|-||+---D--+|x");
        } catch (IOException ex) {
            // do nothing
        }

    }

    @Test
    public void startSniffing_thirdMapUseCaseValid_shouldMatchLettersAndPath() {
        String[] mapLines = fetchMapLines("res/map3.txt");
        assertTrue(mapLines != null);
        Path outputPath = Paths.get(TEST_OUTPUT_URL);
        AsciiMapSniffer asciiMapSniffer = new AsciiMapSniffer(mapLines, outputPath);
        asciiMapSniffer.startSniffing();
        try {
            List<String> outputLines = Files.readAllLines(
                    outputPath,
                    Charset.defaultCharset());
            assertTrue(outputLines.size() == 2);
            assertEquals("BEEFCAKE", outputLines.get(0).trim());
            assertEquals("@---+B||E--+|E|+--F--+|C|||A--|-----K|||+--E--Ex", outputLines.get(1).trim());
        } catch (IOException ex) {
            // do nothing
        }
    }

    private String[] fetchMapLines(String input) {
        String[] mapLines = null;
        try {
            mapLines = Files.readAllLines(Paths.get(input)).toArray(new String[0]);
        } catch (IOException ex) {
            System.out.println("Can't read " + input);
            return null;
        }
        return mapLines;
    }

}
