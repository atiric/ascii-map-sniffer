package com.softwaresauna.asciimap.implementation;

import com.softwaresauna.asciimap.model.Coordinate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Armin
 */
public class AsciiMapImplTest {

    String[] firstMap;

    String[] secondMap;

    String[] thirdMap;

    @Before
    public void setUp() {
        firstMap = new String[]{
              "  @---A---+ "
            , "          | "
            , "  x-B-+   C "
            , "      |   | "
            , "      +---+"
        };

        secondMap = new String[]{
              "  @ "
            , "  | C----+ "
            , "  A |    | "
            , "  +---B--+ "
            , "    |      x "
            , "    |      | "
            , "    +---D--+"
        };

        thirdMap = new String[]{
              "  @---+ "
            , "      B "
            , "K-----|--A "
            , "|     |  | "
            , "|  +--E  | "
            , "|  |     | "
            , "+--E--Ex C "
            , "   |     | "
            , "   +--F--+"
        };

    }
    
    @Test
    public void checkIfStartingCoordinateForFirstMapIsValid(){
        AsciiMapImpl asciMap = new AsciiMapImpl(firstMap);
        Assert.assertEquals( asciMap.getCurrentCoordinate(), new Coordinate<>(0, 2));
    }
    
    @Test
    public void checkIfStartingCoordinateForSecondMapIsValid(){
        AsciiMapImpl asciMap = new AsciiMapImpl(secondMap);
        Assert.assertEquals( asciMap.getCurrentCoordinate(), new Coordinate<>(0, 2));
    }
    
    @Test
    public void checkIfStartingCoordinateForThirdMapIsValid(){
        AsciiMapImpl asciMap = new AsciiMapImpl(thirdMap);
        Assert.assertEquals( asciMap.getCurrentCoordinate(), new Coordinate<>(0, 2));
    }
    

}
