package com.softwaresauna.asciimap.implementation;

import com.softwaresauna.asciimap.model.AsciiMap;
import com.softwaresauna.asciimap.model.Coordinate;
import com.softwaresauna.asciimap.model.Direction;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 * Using naming convention [the name of the tested method]_[expected input /
 * tested state]_[expected behavior].
 * @author Armin
 */
public class AsciiMapRecieverTest {
    
    String[] secondMap;
    AsciiMap map;
    AsciiMapReciever reciever;
    
    @Before
    public void setUp() {
        secondMap = new String[]{
            "  @"
           ,"  | C----+"
           ,"  A |    |"
           ,"  +---B--+"
           ,"    |      x"
           ,"    |      |"
           ,"    +---D--+"
        };
        
        map = new AsciiMapImpl(secondMap);
        reciever = new AsciiMapReciever(map);
    }
    
    
    @Test
    public void getCurrentCharacter_asciiMapInitialized_startingCharacterIsCorrect(){
        AsciiMapReciever asciiMapReciever = new AsciiMapReciever(map);
        assertEquals('@', asciiMapReciever.getCurrentCharacter()); 
    }
    
    @Test
    public void determineDirection_lastDirectionProvidedForUp_correctDirectionDetermined(){
        map.setCurrentCoordinate(new Coordinate<>(4,4));
        map.setCurrentDirection(Direction.UP);
        Direction direction = reciever.determineDirection();
        assertEquals(Direction.UP, direction);
        map.setCurrentCoordinate(new Coordinate<>(5,4));
        map.setCurrentDirection(Direction.UP);
        direction = reciever.determineDirection();
        assertEquals(Direction.UP, direction);
        map.setCurrentCoordinate(new Coordinate<>(3,4));
        map.setCurrentDirection(Direction.UP);
        direction = reciever.determineDirection();
        assertEquals(Direction.UP, direction);
        map.setCurrentCoordinate(new Coordinate<>(2,9));
        map.setCurrentDirection(Direction.UP);
        direction = reciever.determineDirection();
        assertEquals(Direction.UP, direction);
    }
    
    @Test
    public void determineDirection_lastDirectionProvidedForDown_correctDirectionDetermined(){
        map.setCurrentCoordinate(new Coordinate<>(3,4));
        char currentCharacter = reciever.getCurrentCharacter();
        map.setCurrentDirection(Direction.DOWN);
        Direction direction = reciever.determineDirection();
        assertEquals(Direction.DOWN, direction);
        map.setCurrentCoordinate(new Coordinate<>(4,4));
        map.setCurrentDirection(Direction.DOWN);
        direction = reciever.determineDirection();
        assertEquals(Direction.DOWN, direction);
        map.setCurrentCoordinate(new Coordinate<>(2,2));
        map.setCurrentDirection(Direction.DOWN);
        direction = reciever.determineDirection();
        assertEquals(Direction.DOWN, direction);
        map.setCurrentCoordinate(new Coordinate<>(1,2));
        map.setCurrentDirection(Direction.DOWN);
        direction = reciever.determineDirection();
        assertEquals(Direction.DOWN, direction);
    }
    
    @Test
    public void determineDirection_lastDirectionProvidedForRight_correctDirectionDetermined(){
        map.setCurrentCoordinate(new Coordinate<>(3,3));
        char currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == '-' );
        map.setCurrentDirection(Direction.RIGHT);
        Direction direction = reciever.determineDirection();
        assertEquals(Direction.RIGHT, direction);
        map.setCurrentCoordinate(new Coordinate<>(3,5));
        currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == '-' );
        map.setCurrentDirection(Direction.RIGHT);
        direction = reciever.determineDirection();
        assertEquals(Direction.RIGHT, direction);
        map.setCurrentCoordinate(new Coordinate<>(3,6));
        currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == 'B' );
        map.setCurrentDirection(Direction.RIGHT);
        direction = reciever.determineDirection();
        assertEquals(Direction.RIGHT, direction);
        map.setCurrentCoordinate(new Coordinate<>(3,8));
        currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == '-' );
        map.setCurrentDirection(Direction.RIGHT);
        direction = reciever.determineDirection();
        assertEquals(Direction.RIGHT, direction);
    }
    
    
    @Test
    public void determineDirection_lastDirectionProvidedForLeft_correctDirectionDetermined(){
        map.setCurrentCoordinate(new Coordinate<>(3,8));
        char currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == '-' );
        map.setCurrentDirection(Direction.LEFT);
        Direction direction = reciever.determineDirection();
        assertEquals(Direction.LEFT, direction);
        map.setCurrentCoordinate(new Coordinate<>(3,7));
        currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == '-' );
        map.setCurrentDirection(Direction.LEFT);
        direction = reciever.determineDirection();
        assertEquals(Direction.LEFT, direction);
        map.setCurrentCoordinate(new Coordinate<>(3,6));
        currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == 'B' );
        map.setCurrentDirection(Direction.LEFT);
        direction = reciever.determineDirection();
        assertEquals(Direction.LEFT, direction);
        map.setCurrentCoordinate(new Coordinate<>(3,3));
        map.setCurrentDirection(Direction.LEFT);
        direction = reciever.determineDirection();
        assertEquals(Direction.LEFT, direction);
    }
    
    @Test
    public void determineDirection_spotsOfTurningLocations_correctDirectionDetermined(){
        map.setCurrentCoordinate(new Coordinate<>(3,2));
        char currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == '+' );
        map.setCurrentDirection(Direction.DOWN);
        Direction direction = reciever.determineDirection();
        assertEquals(Direction.RIGHT, direction);
        map.setCurrentCoordinate(new Coordinate<>(3,9));
        currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == '+' );
        map.setCurrentDirection(Direction.RIGHT);
        direction = reciever.determineDirection();
        assertEquals(Direction.UP, direction);
        map.setCurrentCoordinate(new Coordinate<>(1,9));
        currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == '+' );
        map.setCurrentDirection(Direction.UP);
        direction = reciever.determineDirection();
        assertEquals(Direction.LEFT, direction);
        map.setCurrentCoordinate(new Coordinate<>(1,4));
        currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == 'C' );
        map.setCurrentDirection(Direction.LEFT);
        direction = reciever.determineDirection();
        assertEquals(Direction.DOWN, direction);
        
    }
    @Test
    public void initializeDirectionOfSearch_mapProvidedWithStartingCharacter_correctDirectionDetermined(){
        map.setCurrentCoordinate(new Coordinate<>(0,2));
        char currentCharacter = reciever.getCurrentCharacter();
        assertTrue(currentCharacter == AsciiMap.STARTING_CHARACTER );
        map.setCurrentDirection(null);
        Direction direction = reciever.determineDirection();
        assertEquals(Direction.DOWN, direction);
    }
    
    @Test 
    public void setMapDirection_directionProvided_directionProvidedIsSet(){
        reciever.setMapDirection(Direction.UP);
        assertTrue(map.getCurrentDirection() == Direction.UP);
    }
    
    @Test
    public void moveInCurrentDirection_directionAndCoordinateProvided_movedInCurrentDirectionOnCorrectCoordinate(){
        reciever.setMapDirection(Direction.DOWN);
        map.setCurrentCoordinate(new Coordinate<>(0,2));
        reciever.moveInCurrentDirection();
        assertTrue( map.getCurrentCoordinate().equals(new Coordinate<>(1,2)));
        assertTrue( reciever.getCurrentCharacter() == '|');
        reciever.setMapDirection(Direction.RIGHT);
        map.setCurrentCoordinate(new Coordinate<>(3,2));
        reciever.moveInCurrentDirection();
        assertTrue( map.getCurrentCoordinate().equals(new Coordinate<>(3,3)));
        assertTrue( reciever.getCurrentCharacter() == '-');
        reciever.setMapDirection(Direction.UP);
        map.setCurrentCoordinate(new Coordinate<>(3,2));
        reciever.moveInCurrentDirection();
        assertTrue( map.getCurrentCoordinate().equals(new Coordinate<>(2,2)));
        assertTrue( reciever.getCurrentCharacter() == 'A');
        reciever.setMapDirection(Direction.LEFT);
        map.setCurrentCoordinate(new Coordinate<>(3,3));
        reciever.moveInCurrentDirection();
        assertTrue( map.getCurrentCoordinate().equals(new Coordinate<>(3,2)));
        assertTrue( reciever.getCurrentCharacter() == '+');
        
    }
    
}
