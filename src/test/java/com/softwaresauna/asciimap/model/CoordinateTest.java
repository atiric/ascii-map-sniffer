package com.softwaresauna.asciimap.model;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Armin
 */
public class CoordinateTest {
    
    
    @Test
    public void hashCodeEqualsCopy_equalCoordinatesProvided_coordinatesAreEqual(){
        Coordinate firstCoordinate = new Coordinate(1,1);
        Coordinate secondCoordinate = new Coordinate(1,1);
        assertEquals(firstCoordinate, secondCoordinate);
        assertEquals(firstCoordinate.toString(), secondCoordinate.toString());
        assertEquals(firstCoordinate.hashCode(), secondCoordinate.hashCode());
        assertNotEquals(firstCoordinate, new Coordinate<>(2,3));
        Coordinate copy = firstCoordinate.copy();
        assertEquals(firstCoordinate, copy);
    }
    
    
}
